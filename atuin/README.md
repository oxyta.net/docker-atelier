# Atuin

https://atuin.sh/

## Configure

```
echo POSTGRES_PASSWORD=$(openssl rand -base64 32 | sed 's=/==g') >> .env
echo ATUIN_OPEN_REGISTRATION=true >> .env
```

## Deploy
```
docker compose up -d
```
